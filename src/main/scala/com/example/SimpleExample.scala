package com.example

/**
 * Created with IntelliJ IDEA.
 * User: nvt
 * Date: 11/2/13
 * Time: 3:37 PM
 */

import arx.Prelude._
import arx.application.easymode.{GraphicsHelper, DeadSimpleConflux}
import arx.core.vec.{Vec4f, Vec2f}
import arx.core.units.UnitOfTime
import arx.gui._
import arx.application.easymode.GraphicsHelper
import arx.gui.KeyPressEvent
import arx.gui.MouseMoveEvent
import arx.gui.MousePressEvent
import arx.gui.MouseReleaseEvent
import arx.gui.KeyReleaseEvent
import arx.application.Application
import org.lwjgl.input.Keyboard
import arx.graphics.helpers.Color

class SimpleExample extends DeadSimpleConflux {
	var stopped = false
	var theta = 0.0f

	var unitAPosition = Vec2f(-6.0f,0.0f)
	var unitBPosition = Vec2f(6.0f,0.0f)

	def draw(graphics: GraphicsHelper) {
		graphics.drawQuad(unitAPosition,Vec2f(5.0f,5.0f),"images/design.png",Vec4f.One)
		graphics.drawQuad(unitBPosition,Vec2f(4.0f,4.0f),"images/design.png",Vec4f.One)
		graphics.drawText(Vec2f(0.0f,0.0f),true,2.0f,"Example Text",Color.Black)
	}


	def tick(timeElapsed: UnitOfTime) {
		if ( ! stopped ) {
			theta += 0.6f * timeElapsed.inSeconds
			unitAPosition = Vec2f( cosf(theta) * 6.0f, sinf(theta) * 6.0f )
			unitBPosition = Vec2f( cosf(theta + Math.PI.toFloat) * 6.0f, sinf(theta + Math.PI.toFloat) * 6.0f )
		}
	}

	def onKeyPress(kpe: KeyPressEvent) {
		if ( kpe.key == Keyboard.KEY_Q ) {
			Application.quit()
		}
	}
	def onKeyRelease(kpe: KeyReleaseEvent) {}
	def onMousePress(mpe: MousePressEvent) {
		stopped = true
	}
	def onMouseRelease(mre: MouseReleaseEvent) {
		stopped = false
	}
	def onMouseMove(mme: MouseMoveEvent) {}
	def onMouseDrag(mde: MouseDragEvent) {}
}

object SimpleExample {
	def main ( args : Array[String] ) {
		Application.start(new SimpleExample)
	}
}