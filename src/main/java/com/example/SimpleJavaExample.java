package com.example;

import arx.application.Application;
import arx.application.easymode.DeadSimpleConflux;
import arx.application.easymode.GraphicsHelper;
import arx.core.units.UnitOfTime;
import arx.core.vec.Vec2f;
import arx.core.vec.Vec4f;
import arx.graphics.Image;
import arx.graphics.helpers.Color;
import arx.gui.*;
import arx.resource.ResourceManager;
import org.lwjgl.input.Keyboard;
import scala.collection.immutable.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: nvt
 * Date: 11/2/13
 * Time: 4:40 PM
 */
public class SimpleJavaExample extends DeadSimpleConflux {
    boolean stopped = false;
    float theta = 0.0f;

    Vec2f unitAPosition = new Vec2f(-6.0f,0.0f);
    Vec2f unitBPosition = new Vec2f(6.0f,0.0f);

    @Override
    public void draw(GraphicsHelper graphics) {
        graphics.drawQuad(unitAPosition,new Vec2f(5.0f,5.0f),"images/design.png",new Vec4f(1.0f,1.0f,1.0f,1.0f));
        graphics.drawQuad(unitBPosition,new Vec2f(4.0f,4.0f),"images/design.png",new Vec4f(1.0f,1.0f,1.0f,1.0f));
        graphics.drawText(new Vec2f(0.0f,0.0f),true,2.0f,"Example Text", Color.Black());
    }

    @Override
    public void onMouseMove(MouseMoveEvent mme) {}

    @Override
    public void onKeyRelease(KeyReleaseEvent kpe) {}

    @Override
    public void onMouseRelease(MouseReleaseEvent mre) {
        stopped = false;
    }

    @Override
    public void tick(UnitOfTime timeElapsed) {
        if ( ! stopped ) {
            theta += 0.6f * timeElapsed.inSeconds();
            unitAPosition = new Vec2f( (float)Math.cos(theta) * 6.0f, (float)Math.sin(theta) * 6.0f );
            unitBPosition = new Vec2f( (float)Math.cos(theta + (float)Math.PI) * 6.0f, (float)Math.sin(theta + (float)Math.PI) * 6.0f );
        }
    }

    @Override
    public void onMousePress(MousePressEvent mpe) {
        stopped = true;
    }

    @Override
    public void onMouseDrag(MouseDragEvent mde) {}

    @Override
    public void onKeyPress(KeyPressEvent kpe) {
        if ( kpe.key() == Keyboard.KEY_Q ) {
            Application.quit();
        }
    }

    public static void main ( String[] args ) {
        Application.start(new SimpleJavaExample(),new HashMap<String, Object>());
    }
}
